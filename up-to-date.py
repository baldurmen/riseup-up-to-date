#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
up-to-date is a program that checks if Riseup's help
pages translations are up to date with the English ones.

Usage:
    up_to_date.py [options] <repository> <output>
    up_to_date.py (-h | --help)

Arguments:
    <repository>    Riseup's help pages repository
    <output>        The file where the final infos are outputted

Options:
    -l language     Filter the languages outputted
    -h  --help      Shows the help screen
"""
import sys

try:
    from docopt import docopt  # Creating command-line interface
except ImportError:
    sys.stderr.write("""
        %s is not installed: this program won't run correctly.
        """ % ("python-docopt"))

import os
import subprocess

from collections import defaultdict


def create_dictionary(args, dictionary):
    """Build the database"""
    for dirname, _, filenames in os.walk(
                                   args["<repository>"],
                                   topdown=False):
        for filename in filenames:
            if os.path.splitext(filename)[1] in [".text", ".md"]:
                filepath = os.path.join(dirname, filename)
                filepath = os.path.abspath(filepath)
                directory = os.path.abspath(args["<repository>"])
                pipe = subprocess.Popen(["git", "--git-dir",
                                         directory + "/.git",
                                         "--work-tree", directory,
                                         "log", "-1", "--format=%at",
                                         "--date=local", "--", filepath],
                                         stdout=subprocess.PIPE)
                time, error = pipe.communicate()
                time = time.decode('ascii') # binary to string
                time = time.strip("\n")
                filename = os.path.splitext(filename)[0]
                if len(filename) == 2 and time != "":            #To fix foo.en.text
                    dictionary[dirname].append([filename, time])
                else:
                    try:
                        dictionary[dirname + "/" + filename.split(".")[0]].append(
                            [filename.split(".")[1], time])
                    except IndexError:  # Skip over uncommitted files
                        continue

    return dictionary


def compare(args, dictionary, f):
    """Compare the timestamps of the files"""
    for directory, values in sorted(dictionary.items()):
        for value in values:
            language, time = value
            if language == "en":
                time_en = time
        for value in values:
            language, time = value
            if args["-l"] == language and time < time_en:
                f.write("{:<70} {:<0} {:<0}".format(directory, language, "\n"))
            if args["-l"] is None and language.isalpha() == True and time < time_en:
                f.write("{:<70} {:<0} {:<0}".format(directory, language, "\n"))


def write_to_output(args, dictionary):
    """Write to the specified output"""
    with open(args["<output>"], "w") as f:
        f.write("{:<67} {:<0} {:<0}".format("Page", "Language", "\n\n"))
        compare(args, dictionary, f)


def main():
    """Main function"""
    args = docopt(__doc__)
    dictionary = defaultdict(list)
    dictionary = create_dictionary(args, dictionary)
    write_to_output(args, dictionary)


if __name__ == "__main__":
    main()
