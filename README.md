`up-to-date` is a program that checks if
[Riseup's help pages translations](https://github.com/riseupnet/riseup_help)
are up to date with the English ones.

The output lists all the pages that needs to be updated. It supports all the
languages the help pages do.

# Installation

First of all, you need Riseup's help pages (duh.) You can clone the git
repository [here](https://github.com/riseupnet/riseup_help). Once this is
done, clone this repository near it.

That's all folks!

# Dependencies

**This program is written in Python 3 and does not support Python 2.7.**

You will need these to run this program correctly:

* python3
* docopt

On Debian distros just type this to install the dependencies:

`sudo aptitude install python3 python3-docopt`

# How to use it

First of all, make sure you have the latest version of the help
pages (no use in knowing if old pages are up to date...).

Once that is done, simply call the program as such:

`python up-to-date.py /path/to/riseup_help /path/to/output`

It is important to point to the git repository itself and not
to the subfolders in it since the programs looks for the .git
there.

If you only want to have the pages in a specific language, use
the `-l` parameter with the proper language code. Language codes
can be found [here](https://github.com/riseupnet/riseup_help#translating).

## CLI options

<pre>
Usage:
    up-to-date.py [options] &lt;repository&gt; &lt;output&gt;
    up-to-date.py (-h | --help)

Arguments:
    &lt;repository&gt;     Riseup's help pages git repository
    &lt;output&gt;         The file where the final infos are outputted

Options:
    -l language      Filter the languages outputted
    -h  --help       Shows the help screen
</pre>
